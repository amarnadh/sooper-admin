var express = require("express");
var app = express();
const bodyParser = require("body-parser"); // Middleware

app.use(express.urlencoded({ extended: true }));

app.use(express.static("public"));
// Using Node.js `require()`

// Using ES6 imports

// use res.render to load up an ejs view file

// index page
app.get("/", function (req, res) {
  res.render("home");
});

// about page
app.get("/auth", function (req, res) {
  var username = req.query.username;
  var password = req.query.password;
  if (username == "superadmin" && password == "password") {
    res.render("superAdmin");
  } else {
    res.send(
      "<script>alert('wrong password');window.location.href = '/';</script>"
    );
  }
});

app.listen(8080);
console.log("Server is listening on port 8080");
